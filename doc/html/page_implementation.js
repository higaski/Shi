var page_implementation =
[
    [ "Stacks", "page_stacks.html", null ],
    [ "Variables", "page_variables.html", null ],
    [ "Dictionary", "page_dictionary.html", null ],
    [ "Extensions", "page_extensions.html", null ],
    [ "Wordlist", "page_wordlist.html", null ],
    [ "Ambiguous conditions", "page_ambiguous_condition.html", null ],
    [ "Interpret loop", "page_interpret.html", null ],
    [ "Control structures", "page_control_structures.html", null ],
    [ "TODO", "page_todo.html", null ]
];