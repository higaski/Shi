var searchData=
[
  ['todo',['TODO',['../page_todo.html',1,'page_implementation']]],
  ['text_5fbegin',['text_begin',['../structShi__init.html#a5ba5274558ae5bc27f41d489e5f8c6f3',1,'Shi_init::text_begin()'],['../structshi_1_1Init.html#ab34a84a6e882901ffa5465ecc54fe849',1,'shi::Init::text_begin()']]],
  ['text_5fend',['text_end',['../structShi__init.html#a6f9f2c9178b9f0b849d8930101047b0d',1,'Shi_init::text_end()'],['../structshi_1_1Init.html#a633b7c925cfaf85443d50d09e61872e1',1,'shi::Init::text_end()']]],
  ['text_5fp2align',['text_p2align',['../structShi__init.html#a7ebb2ab9c24de807968dcc174008f959',1,'Shi_init::text_p2align()'],['../structshi_1_1Init.html#acdf8be699e1e6f22d3c368dce966942c',1,'shi::Init::text_p2align()']]],
  ['tick',['tick',['../namespaceshi.html#aea4ef6bbdc956033df6ebc6e80ae755b',1,'shi::tick(char const *str)'],['../namespaceshi.html#ab4a5bbd8d6e21974db873e00de61ef71',1,'shi::tick(char const *str, size_t len)']]],
  ['todo_2ehpp',['todo.hpp',['../todo_8hpp.html',1,'']]],
  ['top',['top',['../namespaceshi.html#a0b821334775c78326e59d6b571f8af0d',1,'shi']]],
  ['type',['type',['../structshi_1_1remove__cvref.html#ab3045978c4d2558f844c6fd1ae91a67d',1,'shi::remove_cvref']]]
];
