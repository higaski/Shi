var indexSectionsWithContent =
{
  0: "acdefioprstvw",
  1: "irsw",
  2: "s",
  3: "dst",
  4: "cdeiopstvw",
  5: "dfist",
  6: "rtv",
  7: "s",
  8: "acdeistvw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Macros",
  8: "Pages"
};

